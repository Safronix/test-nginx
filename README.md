# test-nginx

Imagine that you have a project that needs an API application and a FRONT application.

We have a nginx and php service for each other

Nginx port for API is 10080 and FRONT's port is 20080

Create network `docker network create mynetwork`

`cd front && make start`

When making call to `http://127.0.0.1:10080` it will return `lala`

When making call to `http://127.0.0.1:20080` it will alternate between `lala` and `ici`